CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers
 * Attention

INTRODUCTION
------------

EVA integrates seamlessly with your web platform to validate
your customers and users email addresses.

 * For a full description of the module visit:
   https://www.drupal.org/project/email_validator

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/email_validator

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the EVA - Email Validator module as you would normally
   install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

  * Configure EVA - Email Validator Settings in
  Administration » Configuration » System » EVA - Email Validator Settings:

   -  General Settings

      Add your "Access Key" generated in https://e-va.io/.

   -  Form Settings

      Check the boxes next to individual forms on which you'd like
      EVA validation be enabled.

   -  Protection Settings

      Select the states that you want to allow.

   -  Log Settings

      Choose if you want to save rejected emails in log.
      Our services provides you 99.9% of uptime,
      however some conections problems could happen.


FAQ
---

  https://e-va.io/faqs

MAINTAINERS
-----------

Current maintainers:
 * Jose Rizal (sbit) - https://www.drupal.org/u/sbit

This project has been sponsored by:
 * Square-Bit
   Specialized in consulting and planning of Drupal powered sites, Square-Bit
   offers installation, development, theming and customization to get you
   started. Visit https://www.square-bit.com for more information.

ATTENTION
---------

  * Before you start, please make sure you have an account at https://e-va.io/

Most bugs have been ironed out, holes covered, features added. This module
is a work in progress. Please report bugs and suggestions, ok?
