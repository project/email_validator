<?php

namespace Drupal\email_validator;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Overrides the email.validator service with the custom EVA - Email validator.
 */
class EmailValidatorServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('email.validator');

    if ($definition->getClass() === EmailValidator::class) {
      $email_validator = $container->getDefinition('email_validator.eva');
      $definition->setClass($email_validator->getClass());
      $definition->setArguments($email_validator->getArguments());
    }
  }

}
