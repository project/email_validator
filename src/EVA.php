<?php

namespace Drupal\email_validator;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Egulias\EmailValidator\Validation\EmailValidation;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;

/**
 * Provides a set of methods to handle validations from the EVA services.
 */
class EVA extends EmailValidator {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The cache.default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $config;

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected Client $httpClient;

  /**
   * Constructs a new EVA object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache.default cache backend.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \GuzzleHttp\Client $http_client
   *   Guzzle\Client instance.
   */
  public function __construct(
    LoggerInterface $logger,
    CacheBackendInterface $cache_backend,
    ConfigFactoryInterface $config,
    Client $http_client
  ) {
    parent::__construct();

    $this->cache = $cache_backend;
    $this->logger = $logger;
    $this->config = $config;
    $this->httpClient = $http_client;

    // Get Configs defined by user.
    $defaults = $this->config->get('email_validator.settings');

    // General Settings.
    $this->url = 'https://e-va.io/api/email/validate/';
    $this->token = $defaults->get('general')['api_key'];

    // Protection Settings.
    $this->mail_states = NULL;

    if (!$defaults->get('protection')['states_all']) {
      $this->mail_states = $defaults->get('protection')['email_states'];
    }

    // Log Settings.
    $this->log_reject = $defaults->get('logs')['wrong_emails'];
    $this->bypass = $defaults->get('logs')['system_down'];
  }

  /**
   * {@inheritdoc}
   */
  public function isValid($email, EmailValidation $email_validation = NULL) {
    parent::isValid($email, $email_validation);

    if (!empty($this->mail_states)) {
      if ($this->curlRequest($email) !== TRUE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Sends an HTTP GET request to retrieve the status for a given email.
   *
   * @param string $email
   *   The email address to validate.
   *
   * @return bool
   *   TRUE or FALSE, depending on if the email is valid or not.
   */
  protected function curlRequest(string $email): bool {
    if (!empty($data = $this->readCacheItem($email))) {
      return $this->checkIsValid($data, $email);
    }

    $options = [
      'headers' => [
        'api-key' => $this->token,
        'CB-ACCESS-TIMESTAMP' => time(),
        'user-agent' => 'Square-bit email validation',
        'content-type' => 'application/json',
      ],
    ];

    try {
      $request = $this->httpClient->request('GET', $this->url . $email, $options);

      if (!empty($response = $request->getBody()->getContents())) {
        $data = Json::decode($response);

        if ($data['status'] === 'nok') {
          $this->logger->warning("EVA can't validate the email @email. Error message: %msg", [
            '@email' => $email,
            '%msg' => $data['message'],
          ]);
        }
        elseif ($data['status'] === 'ok') {
          $this->createCacheItem($data, $email);

          return $this->checkIsValid($data, $email);
        }
      }
    }
    catch (RequestException $e) {
      $this->logger->error('There was a problem with EVA API, after trying to validate the e-mail @email. The error: @error', [
        '@email' => $email,
        '@error' => $e->getMessage(),
      ]);
    }

    return (bool) $this->bypass;
  }

  /**
   * Checks and interprets the status for a given email provided by EVA.
   *
   * @param array $data
   *   The list of response data from EVA.
   * @param string $email
   *   The email to validate.
   *
   * @return bool
   *   TRUE or FALSE, depending on if the email is valid or not.
   */
  protected function checkIsValid(array $data, string $email): bool {
    $email_state = $this->convertEmailStates($data['state']);

    if (is_array($this->mail_states) && in_array($email_state, $this->mail_states)) {
      return TRUE;
    }

    if ($this->log_reject) {
      $this->logger->info('EVA has rejected @email (@state) with the following message: %msg', [
        '@email' => $email,
        '@state' => $data['state'],
        '%msg' => $data['message'],
      ]);
    }

    return FALSE;
  }

  /**
   * Creates a new cache item with specified expiration time.
   *
   * @param mixed $data
   *   The data to be saved on cache.
   * @param string $cid
   *   The cache identifier.
   */
  protected function createCacheItem($data, $cid): void {
    if ($cid) {
      $this->cache->set('email_validator_api:' . $cid, $data, (time() + 3600), []);
    }
  }

  /**
   * Gets a cache data for a provided cache ID.
   *
   * @param string $cid
   *   The cache identifier.
   *
   * @return mixed
   *   The cache data.
   */
  protected function readCacheItem(string $cid) {
    if (!empty($cache = $this->cache->get('email_validator_api:' . $cid))) {
      return $cache->data;
    }

    return NULL;
  }

  /**
   * Converts the readable text states into an int state value.
   *
   * @param string $text_state
   *   The state response text.
   *
   * @return int
   *   A state email status.
   */
  protected function convertEmailStates(string $text_state): int {
    switch ($text_state) {
      case 'Safe':
        $state = 0;
        break;

      case 'Unknown':
        $state = 1;
        break;

      case 'Invalid':
        $state = 2;
        break;

      case 'Risky':
        $state = 3;
        break;

      default:
        $state = 1;
        break;
    }

    return $state;
  }

}
