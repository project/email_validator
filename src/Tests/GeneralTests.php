<?php

namespace Drupal\email_validator\Tests;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\email_validator\EVA;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

/**
 * Basic Tests.
 *
 * @group Cache
 */
abstract class GeneralTests {

  /**
   * The email validator.
   *
   * @var \Drupal\email_validator\EVA
   */
  protected EVA $emailValidator;

  /**
   * Test of validating an email.
   */
  public static function testValidate() {

    $logger = new LoggerInterface();
    $cache = new CacheBackendInterface();
    $config = new ConfigFactoryInterface();
    $http_client = new Client();

    $emailValidator = new EVA($logger, $cache, $config, $http_client);

    $emailValidator->isValid('info@square-bit.com', NULL);
  }

}
