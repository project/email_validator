<?php

namespace Drupal\email_validator\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to show a set of configurable field settings for EVA.
 *
 * @package Drupal\email_validator\Form
 */
class EmailValidatorConfigForm extends ConfigFormBase {

  /**
   * The key for the configuration.
   *
   * @var string
   */
  public const CONFIG_KEY = 'email_validator.settings';

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * EmailValidatorConfigForm constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'email_validator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EmailValidatorConfigForm {
    return new static(
      $container->get('module_handler'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);

    $link_eva = Link::fromTextAndUrl('https://e-va.io/', Url::fromUri('https://e-va.io/', [
      'attributes' => ['target' => '_blank'],
    ]))->toString();

    // - General Settings - API.
    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['general']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key'),
      '#default_value' => $config->get('general.api_key'),
      '#description' => $this->t('Before you start, please make sure you have an account at @link_eva', ['@link_eva' => $link_eva]),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'PLACE_YOUR_KEY_HERE',
      ],
    ];

    // - Protection Settings.
    $form['protection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Protection'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['protection']['states_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable EVA'),
      '#description' => $this->t('Selecting this option all the states below will be allowed. Basically, this means that our service is disabled.'),
      '#default_value' => $config->get('protection.states_all'),
    ];

    $form['protection']['states'] = [
      '#type' => 'container',
      '#states' => [
        'disabled' => [
          'input[name="states_all"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['protection']['states']['email_states'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Email states allowed'),
      '#default_value' => $config->get('protection.email_states'),
      '#options' => [
        0 => $this->t('Safe'),
        1 => $this->t('Unknown'),
        2 => $this->t('Invalid'),
        3 => $this->t('Risky'),
      ],
    ];

    // - Forms Configuration.
    $form['configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Forms'),
      '#description' => $this->t("Specify the form ids and corresponding field names, or even parent containers, where EVA should be enabled. Enter one path per line. An example is user_register_form:mail, where 'user_register_form' is the form ID and 'mail' is the field name."),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $form['configuration']['forms'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Forms'),
      '#title_display' => 'invisible',
      '#default_value' => $config->get('configuration.forms'),
    ];

    /*$form_settings = $config->get('configuration.forms');
    $form['configuration']['forms'] = [
    '#type' => 'container',
    ];

    $form['configuration']['forms']['user_register_form'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('User Registration form'),
    '#default_value' => $this->getFormSettingsValue($form_settings, 'user_register_form'),
    ];

    $form['configuration']['forms']['commerce_checkout_flow_multistep_default'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('Commerce Registration form'),
    '#default_value' => $this->getFormSettingsValue($form_settings, 'commerce_checkout_flow_multistep_default'),
    ];

    $form['configuration']['forms']['webform_submission'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('Webforms - Email field'),
    '#default_value' => $this->getFormSettingsValue($form_settings, 'webform_submission'),
    ];*/

    // - Log Settings.
    $form['logs'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logs'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['logs']['wrong_emails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log rejected emails'),
      '#description' => $this->t('Report information about rejected emails to the log.'),
      '#default_value' => $config->get('logs.wrong_emails'),
    ];

    // Replace the description with a link if dblog.module is enabled.
    if ($this->moduleHandler->moduleExists('dblog')) {
      $form['logs']['wrong_emails']['#description'] = $this->t('Report information about rejected emails to the <a href=":dblog">log</a>.', [
        ':dblog' => Url::fromRoute('dblog.overview')->toString(),
      ]);
    }

    $form['logs']['system_down'] = [
      '#type' => 'radios',
      '#title' => $this->t("When EVA doesn't have credits or the API isn't answering"),
      '#description' => $this->t('Our services provides you 99.9% of uptime, however some connections problems could happen or even your credits may be empty.'),
      '#options' => [
        0 => $this->t('Reject all the validations'),
        1 => $this->t('Bypass all the validations'),
      ],
      '#default_value' => $config->get('logs.system_down'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(self::CONFIG_KEY)
      ->set('general.api_key', $form_state->getValue('api_key'))
      ->set('protection.states_all', $form_state->getValue('states_all'))
      ->set('protection.email_states', $form_state->getValue('email_states'))
      ->set('logs.wrong_emails', $form_state->getValue('wrong_emails'))
      ->set('logs.system_down', $form_state->getValue('system_down'))
      ->set('configuration', $form_state->getValue('configuration'))
      ->save();
  }

  /**
   * Extracts a value from a given list of settings for a given form ID.
   *
   * @param array $form_settings
   * @param string $form_id
   *
   * @return int
   *   An int value if the form ID exists in the form settings or zero if not.
   */
  protected function getFormSettingsValue(array $form_settings, string $form_id): int {
    if (!empty($form_settings) && isset($form_settings[$form_id])) {
      return $form_settings[$form_id];
    }

    return 0;
  }

}
