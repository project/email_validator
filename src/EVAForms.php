<?php

namespace Drupal\email_validator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides an utility service to support the validation of custom forms.
 */
class EVAForms {

  use StringTranslationTrait;
  
  /**
   * Returns a list of configurations defining which forms should use EVA.
   *
   * @return array
   *   A list of form ids and corresponding fields.
   */
  protected static function getConfigurationForms(): array {
    $config_forms = \Drupal::configFactory()
      ->get('email_validator.settings')
      ->get('configuration.forms');

    if (empty($config_forms)) {
      return [];
    }

    $configuration = [];
    $forms_list = str_replace(["\r\n", "\r"], "\n", trim($config_forms));
    $forms_list = explode("\n", $forms_list);

    foreach (array_filter((array) $forms_list) as $expression) {
      [$form_id, $field] = explode(':', $expression);

      if (empty($form_id) || empty($field)) {
        continue;
      }

      $field = explode('.', $field);

      $configuration[] = [
        'form' => $form_id,
        'field' => $field,
      ];
    }

    return $configuration;
  }

  /**
   * Checks if EVA should validate the provided form ID.
   *
   * @param string $form_id
   *   The form ID.
   *
   * @return bool
   *   TRUE or FALSE, depending on if the form should use EVA or not.
   */
  public static function validateForm(string $form_id): bool {
    $config_forms = self::getConfigurationForms();

    if (!empty($config_forms) && in_array($form_id, array_column($config_forms, 'form'))) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Custom validation on forms for 'mail' fields.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function validateCustomForm(array &$form, FormStateInterface $form_state) {
    $config_forms = self::getConfigurationForms();
    $key = array_search($form['#form_id'], array_column($config_forms, 'form'));

    if (empty($config_forms[$key])) {
      return;
    }

    $value = NULL;
    $field = $config_forms[$key]['field'];

    if ($config_forms[$key]['form'] === 'commerce_checkout_flow_multistep_default'
      && !empty($commerce = $form_state->getValue('contact_information'))
      && !empty($commerce['email'])) {
      $value = $commerce['email'];
    }
    elseif (strpos($config_forms[$key]['form'], 'webform_submission') !== FALSE) {
      $value = $form_state->getValues('elements');
      $value = $value[$field] ?? NULL;
    }
    elseif (!empty($form_state->getValue($field))) {
      $value = $form_state->getValue($field);
    }

    if ($value) {
      if (!\Drupal::service('email_validator.eva')->isValid($value, NULL)) {
        $form_state->setErrorByName(implode('][', $field), \Drupal::translation()->translate('The email address @mail is not valid.', [
          '@mail' => $value,
        ]));
      }
    }
  }

}
