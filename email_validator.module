<?php

/**
 * @file
 * Email_validator.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\email_validator\EVAForms;

/**
 * Implements hook_help().
 */
function email_validator_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name === 'help.page.email_validator') {
    $text = file_get_contents(dirname(__FILE__) . '/README.md');

    if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
      return '<pre>' . $text . '</pre>';
    }
    else {
      $filter_manager = \Drupal::service('plugin.manager.filter');
      $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
      $config = ['settings' => $settings];
      $filter = $filter_manager->createInstance('markdown', $config);

      return $filter->process($text, 'en');
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 * - Reads EVA settings and check if the mails for the given form should be
 * validated.
 */
function email_validator_form_alter(&$form, &$form_state, $form_id) {
  if (EVAForms::validateForm($form_id)) {
    $form['#validate'][] = '\Drupal\email_validator\EVAForms::validateCustomForm';
  }
}
